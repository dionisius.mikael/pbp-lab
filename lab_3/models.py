from django.db import models

# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    DOB = models.DateField()
