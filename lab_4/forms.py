from django import forms
from .models import Note

# class DateInput(forms.DateInput):
#     input_type = 'date'

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        # widgets = {'DOB': DateInput()}
