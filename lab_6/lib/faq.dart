import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Remove the debug banner
        debugShowCheckedModeBanner: false,
        title: 'PDP D-05',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
        ),
        home: HomePage());
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // Generating some dummy data
  List<Map<String, dynamic>> _items = List.generate(
      20,
          (index) => {
        'id': index,
        'title': 'Pertanyaan $index',
        'description':
        'Ini adalah jawaban dari pertanyaan $index.',
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff5297ee),
          centerTitle: true,
          title: Text('Frequently Asked Questions (FAQs)',
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold
            ),
          ),
        ),
        body:
        SingleChildScrollView(
          child: ExpansionPanelList.radio(
            elevation: 3,
            animationDuration: Duration(milliseconds: 600),
            children: _items
                .map(
                  (item) => ExpansionPanelRadio(
                value: item['id'],
                canTapOnHeader: true,
                headerBuilder: (_, isExpanded) => Container(
                    padding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                    child: Text(
                      item['title'],
                      style: TextStyle(fontSize: 20),
                    )),
                body: Container(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                  child: Text(item['description']),
                ),
              ),
            )
                .toList(),
          ),
        )
    );
  }
}